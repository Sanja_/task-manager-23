package ru.karamyshev.taskmanager.bootstrap;


import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.api.ICommandRepository;
import ru.karamyshev.taskmanager.api.ICommandService;
import ru.karamyshev.taskmanager.api.IServiceLocator;
import ru.karamyshev.taskmanager.api.ISessionService;
import ru.karamyshev.taskmanager.command.AbstractCommand;

import ru.karamyshev.taskmanager.constant.MsgCommandConst;
import ru.karamyshev.taskmanager.endpoint.*;
import ru.karamyshev.taskmanager.exception.CommandIncorrectException;
import ru.karamyshev.taskmanager.repository.CommandRepository;
import ru.karamyshev.taskmanager.service.CommandService;
import ru.karamyshev.taskmanager.service.SessionService;
import ru.karamyshev.taskmanager.util.TerminalUtil;


import java.lang.Exception;
import java.util.*;


public class Bootstrap implements IServiceLocator {

    private final ISessionService sessionService = new SessionService();

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final AuthenticationEndpointService authenticationEndpointService = new AuthenticationEndpointService();

    private final AuthenticationEndpoint authenticationEndpoint = authenticationEndpointService.getAuthenticationEndpointPort();

    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    private final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    private final TaskEndpointService taskEndpointService = new TaskEndpointService();

    private final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    private final UserEndpointService userEndpointService = new UserEndpointService();

    private final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();

    private final AdminEndpointService adminEndpointService = new AdminEndpointService();

    private final AdminEndpoint adminEndpoint = adminEndpointService.getAdminEndpointPort();

    private final AdminUserEndpointService adminUserEndpointService = new AdminUserEndpointService();

    private final AdminUserEndpoint adminUserEndpoint = adminUserEndpointService.getAdminUserEndpointPort();

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    {
        init(commandService.getTerminalCommands());
    }

    private void init(List<AbstractCommand> commands){
        for (AbstractCommand command: commands) {
            registry(command);
        }
    }

    public void run(final String[] args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        parsArgs(args);
        inputCommand();
    }

    @SneakyThrows
    private void inputCommand() {
        while (true) {
            try {
                parsCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
            }
        }
    }

    private void registry(final AbstractCommand command){
        if (command == null) return;
        command.setServiceLocator(this);
        commands.put(command.name(), command);
        arguments.put(command.arg(), command);
    }

    private void parsCommand(final String args) {
        validateArgs(args);
        String[] command = args.trim().split("\\s+");
        for (String arg : command) chooseResponsCommand(arg);
    }

    private void parsArgs(final String... args) {
        validateArgs(args);
        try {
            for (String arg : args) chooseResponsArg(arg.trim());
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println("[FAIL]");
        }

    }

    private void validateArgs(final String... args) {
        if (args != null || args.length > 0) return;
        System.out.println(MsgCommandConst.COMMAND_ABSENT);
    }

    @SneakyThrows
    private void chooseResponsArg(final String arg) {
        final AbstractCommand argument =  arguments.get(arg);
        if (argument == null) throw new CommandIncorrectException(arg);
        argument.execute();
    }

    @SneakyThrows
    private void chooseResponsCommand(final String cmd){
        final AbstractCommand command =  commands.get(cmd);
        if (command == null) throw new CommandIncorrectException(cmd);
        command.execute();
    }


    @NotNull
    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @NotNull
    @Override
    public ISessionService getSessionService() {
        return sessionService;
    }

    @NotNull
    @Override
    public SessionEndpoint getSessionEndpoint() {
        return sessionEndpoint;
    }

    @NotNull
    @Override
    public AdminEndpoint getAdminEndpoint() {
        return adminEndpoint;
    }

    @NotNull
    @Override
    public AdminUserEndpoint getAdminUserEndpoint() {
        return adminUserEndpoint;
    }

    @NotNull
    @Override
    public TaskEndpoint getTaskEndpoint() {
        return taskEndpoint;
    }

    @NotNull
    @Override
    public ProjectEndpoint getProjectEndpoint() {
        return projectEndpoint;
    }

    @NotNull
    @Override
    public UserEndpoint getUserEndpoint() {
        return userEndpoint;
    }

    @NotNull
    @Override
    public AuthenticationEndpoint getAuthenticationEndpoint() {
        return authenticationEndpoint;
    }

}
