
package ru.karamyshev.taskmanager.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.karamyshev.taskmanager.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Exception_QNAME = new QName("http://endpoint.taskmanager.karamyshev.ru/", "Exception");
    private final static QName _CreateUser_QNAME = new QName("http://endpoint.taskmanager.karamyshev.ru/", "createUser");
    private final static QName _CreateUserEmail_QNAME = new QName("http://endpoint.taskmanager.karamyshev.ru/", "createUserEmail");
    private final static QName _CreateUserEmailResponse_QNAME = new QName("http://endpoint.taskmanager.karamyshev.ru/", "createUserEmailResponse");
    private final static QName _CreateUserResponse_QNAME = new QName("http://endpoint.taskmanager.karamyshev.ru/", "createUserResponse");
    private final static QName _CreateUserRole_QNAME = new QName("http://endpoint.taskmanager.karamyshev.ru/", "createUserRole");
    private final static QName _CreateUserRoleResponse_QNAME = new QName("http://endpoint.taskmanager.karamyshev.ru/", "createUserRoleResponse");
    private final static QName _FindUserById_QNAME = new QName("http://endpoint.taskmanager.karamyshev.ru/", "findUserById");
    private final static QName _FindUserByIdResponse_QNAME = new QName("http://endpoint.taskmanager.karamyshev.ru/", "findUserByIdResponse");
    private final static QName _FindUserByLogin_QNAME = new QName("http://endpoint.taskmanager.karamyshev.ru/", "findUserByLogin");
    private final static QName _FindUserByLoginResponse_QNAME = new QName("http://endpoint.taskmanager.karamyshev.ru/", "findUserByLoginResponse");
    private final static QName _GetUserList_QNAME = new QName("http://endpoint.taskmanager.karamyshev.ru/", "getUserList");
    private final static QName _GetUserListResponse_QNAME = new QName("http://endpoint.taskmanager.karamyshev.ru/", "getUserListResponse");
    private final static QName _LoadUser_QNAME = new QName("http://endpoint.taskmanager.karamyshev.ru/", "loadUser");
    private final static QName _LoadUserResponse_QNAME = new QName("http://endpoint.taskmanager.karamyshev.ru/", "loadUserResponse");
    private final static QName _LockUserByLogin_QNAME = new QName("http://endpoint.taskmanager.karamyshev.ru/", "lockUserByLogin");
    private final static QName _LockUserByLoginResponse_QNAME = new QName("http://endpoint.taskmanager.karamyshev.ru/", "lockUserByLoginResponse");
    private final static QName _RemoveByLogin_QNAME = new QName("http://endpoint.taskmanager.karamyshev.ru/", "removeByLogin");
    private final static QName _RemoveByLoginResponse_QNAME = new QName("http://endpoint.taskmanager.karamyshev.ru/", "removeByLoginResponse");
    private final static QName _RemoveUser_QNAME = new QName("http://endpoint.taskmanager.karamyshev.ru/", "removeUser");
    private final static QName _RemoveUserById_QNAME = new QName("http://endpoint.taskmanager.karamyshev.ru/", "removeUserById");
    private final static QName _RemoveUserByIdResponse_QNAME = new QName("http://endpoint.taskmanager.karamyshev.ru/", "removeUserByIdResponse");
    private final static QName _RemoveUserByLogin_QNAME = new QName("http://endpoint.taskmanager.karamyshev.ru/", "removeUserByLogin");
    private final static QName _RemoveUserByLoginResponse_QNAME = new QName("http://endpoint.taskmanager.karamyshev.ru/", "removeUserByLoginResponse");
    private final static QName _RemoveUserResponse_QNAME = new QName("http://endpoint.taskmanager.karamyshev.ru/", "removeUserResponse");
    private final static QName _UnlockUserByLogin_QNAME = new QName("http://endpoint.taskmanager.karamyshev.ru/", "unlockUserByLogin");
    private final static QName _UnlockUserByLoginResponse_QNAME = new QName("http://endpoint.taskmanager.karamyshev.ru/", "unlockUserByLoginResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.karamyshev.taskmanager.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link CreateUser }
     * 
     */
    public CreateUser createCreateUser() {
        return new CreateUser();
    }

    /**
     * Create an instance of {@link CreateUserEmail }
     * 
     */
    public CreateUserEmail createCreateUserEmail() {
        return new CreateUserEmail();
    }

    /**
     * Create an instance of {@link CreateUserEmailResponse }
     * 
     */
    public CreateUserEmailResponse createCreateUserEmailResponse() {
        return new CreateUserEmailResponse();
    }

    /**
     * Create an instance of {@link CreateUserResponse }
     * 
     */
    public CreateUserResponse createCreateUserResponse() {
        return new CreateUserResponse();
    }

    /**
     * Create an instance of {@link CreateUserRole }
     * 
     */
    public CreateUserRole createCreateUserRole() {
        return new CreateUserRole();
    }

    /**
     * Create an instance of {@link CreateUserRoleResponse }
     * 
     */
    public CreateUserRoleResponse createCreateUserRoleResponse() {
        return new CreateUserRoleResponse();
    }

    /**
     * Create an instance of {@link FindUserById }
     * 
     */
    public FindUserById createFindUserById() {
        return new FindUserById();
    }

    /**
     * Create an instance of {@link FindUserByIdResponse }
     * 
     */
    public FindUserByIdResponse createFindUserByIdResponse() {
        return new FindUserByIdResponse();
    }

    /**
     * Create an instance of {@link FindUserByLogin }
     * 
     */
    public FindUserByLogin createFindUserByLogin() {
        return new FindUserByLogin();
    }

    /**
     * Create an instance of {@link FindUserByLoginResponse }
     * 
     */
    public FindUserByLoginResponse createFindUserByLoginResponse() {
        return new FindUserByLoginResponse();
    }

    /**
     * Create an instance of {@link GetUserList }
     * 
     */
    public GetUserList createGetUserList() {
        return new GetUserList();
    }

    /**
     * Create an instance of {@link GetUserListResponse }
     * 
     */
    public GetUserListResponse createGetUserListResponse() {
        return new GetUserListResponse();
    }

    /**
     * Create an instance of {@link LoadUser }
     * 
     */
    public LoadUser createLoadUser() {
        return new LoadUser();
    }

    /**
     * Create an instance of {@link LoadUserResponse }
     * 
     */
    public LoadUserResponse createLoadUserResponse() {
        return new LoadUserResponse();
    }

    /**
     * Create an instance of {@link LockUserByLogin }
     * 
     */
    public LockUserByLogin createLockUserByLogin() {
        return new LockUserByLogin();
    }

    /**
     * Create an instance of {@link LockUserByLoginResponse }
     * 
     */
    public LockUserByLoginResponse createLockUserByLoginResponse() {
        return new LockUserByLoginResponse();
    }

    /**
     * Create an instance of {@link RemoveByLogin }
     * 
     */
    public RemoveByLogin createRemoveByLogin() {
        return new RemoveByLogin();
    }

    /**
     * Create an instance of {@link RemoveByLoginResponse }
     * 
     */
    public RemoveByLoginResponse createRemoveByLoginResponse() {
        return new RemoveByLoginResponse();
    }

    /**
     * Create an instance of {@link RemoveUser }
     * 
     */
    public RemoveUser createRemoveUser() {
        return new RemoveUser();
    }

    /**
     * Create an instance of {@link RemoveUserById }
     * 
     */
    public RemoveUserById createRemoveUserById() {
        return new RemoveUserById();
    }

    /**
     * Create an instance of {@link RemoveUserByIdResponse }
     * 
     */
    public RemoveUserByIdResponse createRemoveUserByIdResponse() {
        return new RemoveUserByIdResponse();
    }

    /**
     * Create an instance of {@link RemoveUserByLogin }
     * 
     */
    public RemoveUserByLogin createRemoveUserByLogin() {
        return new RemoveUserByLogin();
    }

    /**
     * Create an instance of {@link RemoveUserByLoginResponse }
     * 
     */
    public RemoveUserByLoginResponse createRemoveUserByLoginResponse() {
        return new RemoveUserByLoginResponse();
    }

    /**
     * Create an instance of {@link RemoveUserResponse }
     * 
     */
    public RemoveUserResponse createRemoveUserResponse() {
        return new RemoveUserResponse();
    }

    /**
     * Create an instance of {@link UnlockUserByLogin }
     * 
     */
    public UnlockUserByLogin createUnlockUserByLogin() {
        return new UnlockUserByLogin();
    }

    /**
     * Create an instance of {@link UnlockUserByLoginResponse }
     * 
     */
    public UnlockUserByLoginResponse createUnlockUserByLoginResponse() {
        return new UnlockUserByLoginResponse();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link AbstractEntitty }
     * 
     */
    public AbstractEntitty createAbstractEntitty() {
        return new AbstractEntitty();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.karamyshev.ru/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.karamyshev.ru/", name = "createUser")
    public JAXBElement<CreateUser> createCreateUser(CreateUser value) {
        return new JAXBElement<CreateUser>(_CreateUser_QNAME, CreateUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserEmail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.karamyshev.ru/", name = "createUserEmail")
    public JAXBElement<CreateUserEmail> createCreateUserEmail(CreateUserEmail value) {
        return new JAXBElement<CreateUserEmail>(_CreateUserEmail_QNAME, CreateUserEmail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserEmailResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.karamyshev.ru/", name = "createUserEmailResponse")
    public JAXBElement<CreateUserEmailResponse> createCreateUserEmailResponse(CreateUserEmailResponse value) {
        return new JAXBElement<CreateUserEmailResponse>(_CreateUserEmailResponse_QNAME, CreateUserEmailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.karamyshev.ru/", name = "createUserResponse")
    public JAXBElement<CreateUserResponse> createCreateUserResponse(CreateUserResponse value) {
        return new JAXBElement<CreateUserResponse>(_CreateUserResponse_QNAME, CreateUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserRole }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.karamyshev.ru/", name = "createUserRole")
    public JAXBElement<CreateUserRole> createCreateUserRole(CreateUserRole value) {
        return new JAXBElement<CreateUserRole>(_CreateUserRole_QNAME, CreateUserRole.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserRoleResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.karamyshev.ru/", name = "createUserRoleResponse")
    public JAXBElement<CreateUserRoleResponse> createCreateUserRoleResponse(CreateUserRoleResponse value) {
        return new JAXBElement<CreateUserRoleResponse>(_CreateUserRoleResponse_QNAME, CreateUserRoleResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.karamyshev.ru/", name = "findUserById")
    public JAXBElement<FindUserById> createFindUserById(FindUserById value) {
        return new JAXBElement<FindUserById>(_FindUserById_QNAME, FindUserById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.karamyshev.ru/", name = "findUserByIdResponse")
    public JAXBElement<FindUserByIdResponse> createFindUserByIdResponse(FindUserByIdResponse value) {
        return new JAXBElement<FindUserByIdResponse>(_FindUserByIdResponse_QNAME, FindUserByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByLogin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.karamyshev.ru/", name = "findUserByLogin")
    public JAXBElement<FindUserByLogin> createFindUserByLogin(FindUserByLogin value) {
        return new JAXBElement<FindUserByLogin>(_FindUserByLogin_QNAME, FindUserByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByLoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.karamyshev.ru/", name = "findUserByLoginResponse")
    public JAXBElement<FindUserByLoginResponse> createFindUserByLoginResponse(FindUserByLoginResponse value) {
        return new JAXBElement<FindUserByLoginResponse>(_FindUserByLoginResponse_QNAME, FindUserByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.karamyshev.ru/", name = "getUserList")
    public JAXBElement<GetUserList> createGetUserList(GetUserList value) {
        return new JAXBElement<GetUserList>(_GetUserList_QNAME, GetUserList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.karamyshev.ru/", name = "getUserListResponse")
    public JAXBElement<GetUserListResponse> createGetUserListResponse(GetUserListResponse value) {
        return new JAXBElement<GetUserListResponse>(_GetUserListResponse_QNAME, GetUserListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.karamyshev.ru/", name = "loadUser")
    public JAXBElement<LoadUser> createLoadUser(LoadUser value) {
        return new JAXBElement<LoadUser>(_LoadUser_QNAME, LoadUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.karamyshev.ru/", name = "loadUserResponse")
    public JAXBElement<LoadUserResponse> createLoadUserResponse(LoadUserResponse value) {
        return new JAXBElement<LoadUserResponse>(_LoadUserResponse_QNAME, LoadUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LockUserByLogin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.karamyshev.ru/", name = "lockUserByLogin")
    public JAXBElement<LockUserByLogin> createLockUserByLogin(LockUserByLogin value) {
        return new JAXBElement<LockUserByLogin>(_LockUserByLogin_QNAME, LockUserByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LockUserByLoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.karamyshev.ru/", name = "lockUserByLoginResponse")
    public JAXBElement<LockUserByLoginResponse> createLockUserByLoginResponse(LockUserByLoginResponse value) {
        return new JAXBElement<LockUserByLoginResponse>(_LockUserByLoginResponse_QNAME, LockUserByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveByLogin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.karamyshev.ru/", name = "removeByLogin")
    public JAXBElement<RemoveByLogin> createRemoveByLogin(RemoveByLogin value) {
        return new JAXBElement<RemoveByLogin>(_RemoveByLogin_QNAME, RemoveByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveByLoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.karamyshev.ru/", name = "removeByLoginResponse")
    public JAXBElement<RemoveByLoginResponse> createRemoveByLoginResponse(RemoveByLoginResponse value) {
        return new JAXBElement<RemoveByLoginResponse>(_RemoveByLoginResponse_QNAME, RemoveByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.karamyshev.ru/", name = "removeUser")
    public JAXBElement<RemoveUser> createRemoveUser(RemoveUser value) {
        return new JAXBElement<RemoveUser>(_RemoveUser_QNAME, RemoveUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.karamyshev.ru/", name = "removeUserById")
    public JAXBElement<RemoveUserById> createRemoveUserById(RemoveUserById value) {
        return new JAXBElement<RemoveUserById>(_RemoveUserById_QNAME, RemoveUserById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.karamyshev.ru/", name = "removeUserByIdResponse")
    public JAXBElement<RemoveUserByIdResponse> createRemoveUserByIdResponse(RemoveUserByIdResponse value) {
        return new JAXBElement<RemoveUserByIdResponse>(_RemoveUserByIdResponse_QNAME, RemoveUserByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserByLogin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.karamyshev.ru/", name = "removeUserByLogin")
    public JAXBElement<RemoveUserByLogin> createRemoveUserByLogin(RemoveUserByLogin value) {
        return new JAXBElement<RemoveUserByLogin>(_RemoveUserByLogin_QNAME, RemoveUserByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserByLoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.karamyshev.ru/", name = "removeUserByLoginResponse")
    public JAXBElement<RemoveUserByLoginResponse> createRemoveUserByLoginResponse(RemoveUserByLoginResponse value) {
        return new JAXBElement<RemoveUserByLoginResponse>(_RemoveUserByLoginResponse_QNAME, RemoveUserByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.karamyshev.ru/", name = "removeUserResponse")
    public JAXBElement<RemoveUserResponse> createRemoveUserResponse(RemoveUserResponse value) {
        return new JAXBElement<RemoveUserResponse>(_RemoveUserResponse_QNAME, RemoveUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnlockUserByLogin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.karamyshev.ru/", name = "unlockUserByLogin")
    public JAXBElement<UnlockUserByLogin> createUnlockUserByLogin(UnlockUserByLogin value) {
        return new JAXBElement<UnlockUserByLogin>(_UnlockUserByLogin_QNAME, UnlockUserByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnlockUserByLoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.karamyshev.ru/", name = "unlockUserByLoginResponse")
    public JAXBElement<UnlockUserByLoginResponse> createUnlockUserByLoginResponse(UnlockUserByLoginResponse value) {
        return new JAXBElement<UnlockUserByLoginResponse>(_UnlockUserByLoginResponse_QNAME, UnlockUserByLoginResponse.class, null, value);
    }

}
