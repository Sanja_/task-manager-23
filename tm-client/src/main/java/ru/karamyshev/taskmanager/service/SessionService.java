package ru.karamyshev.taskmanager.service;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.ISessionService;
import ru.karamyshev.taskmanager.endpoint.Session;

public class SessionService implements ISessionService {

    private Session session;

    @Override
    public void setSession(Session session) {
        this.session = session;
    }

    @Override
    public void clearSession() {
        session = null;
    }

    @Nullable
    @Override
    public Session getSession() {
        return session;
    }

}
