package ru.karamyshev.taskmanager.api;

import org.jetbrains.annotations.NotNull;

import ru.karamyshev.taskmanager.endpoint.*;

public interface IServiceLocator {

     @NotNull
     ICommandService getCommandService();

     @NotNull
     ISessionService getSessionService();

     @NotNull
     SessionEndpoint getSessionEndpoint();

     @NotNull
     AdminEndpoint getAdminEndpoint();

     @NotNull
     AdminUserEndpoint getAdminUserEndpoint();

     @NotNull
     TaskEndpoint getTaskEndpoint();

     @NotNull
     ProjectEndpoint getProjectEndpoint();

     @NotNull
     UserEndpoint getUserEndpoint();

     @NotNull
     AuthenticationEndpoint getAuthenticationEndpoint();
}
