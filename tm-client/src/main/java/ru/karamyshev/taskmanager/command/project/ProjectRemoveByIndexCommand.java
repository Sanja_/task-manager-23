package ru.karamyshev.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.endpoint.Project;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.util.TerminalUtil;

public class ProjectRemoveByIndexCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-prtrmvind";
    }

    @NotNull
    @Override
    public String name() {
        return "project-remove-by-index";
    }

    @Override
    public @NotNull String description() {
        return "Remove project by index.";
    }

    @Override
    public void execute() throws Exception {
        final Session session = serviceLocator.getSessionService().getSession();
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER PROJECT INDEX FOR DELETION:");
        final Integer index = TerminalUtil.nextNumber();
        final Project project = serviceLocator.getProjectEndpoint().removeOneProjectByIndex(session, index);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
