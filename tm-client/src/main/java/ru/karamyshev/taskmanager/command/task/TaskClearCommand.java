package ru.karamyshev.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.endpoint.TaskEndpoint;

public class TaskClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-tskclr";
    }

    @NotNull
    @Override
    public String name() {
        return "task-clear";
    }

    @Override
    public @NotNull String description() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CLEAR TASKS]");
        final Session session = serviceLocator.getSessionService().getSession();
        TaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        taskEndpoint.clearTask(session);
        System.out.println("[OK]");
    }

    @NotNull
    public Role[] roles(){
        return new Role[] { Role.USER, Role.ADMIN };
    }

}
