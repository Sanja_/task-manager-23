package ru.karamyshev.taskmanager.command.system;

import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.util.TerminalUtil;

public class RegistryUserCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-rgstr";
    }

    @NotNull
    @Override
    public String name() {
        return "registry";
    }

    @Override
    public @NotNull String description() {
        return "Registration new account.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REGISTRY]");
        System.out.println("ENTER LOGIN");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL");
        final String email = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAdminUserEndpoint().createUserWithEmail(login, password, email);
        System.out.println("[OK]");
    }

}
