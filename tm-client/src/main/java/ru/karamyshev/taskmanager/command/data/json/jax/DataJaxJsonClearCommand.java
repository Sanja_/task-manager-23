package ru.karamyshev.taskmanager.command.data.json.jax;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.command.AbstractDataCommand;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;

import java.io.Serializable;

public class DataJaxJsonClearCommand extends AbstractDataCommand implements Serializable {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-jax-json-clear";
    }

    @Override
    public @NotNull String description() {
        return "Remove json(jax-b) file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[REMOVE JSON(JAX-V) FILE]");
        final Session session = serviceLocator.getSessionService().getSession();
        serviceLocator.getAdminEndpoint().clearDataJaxBJson(session);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
