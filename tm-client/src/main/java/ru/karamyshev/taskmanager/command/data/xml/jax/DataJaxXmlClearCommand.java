package ru.karamyshev.taskmanager.command.data.xml.jax;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.command.AbstractDataCommand;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;

public class DataJaxXmlClearCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-jax-xml-clear";
    }

    @Override
    public @NotNull String description() {
        return "Remove json(faster) file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[REMOVE XML(JAX-B) FILE]");
        final Session session = serviceLocator.getSessionService().getSession();
        serviceLocator.getAdminEndpoint().clearDataJaxBXml(session);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
