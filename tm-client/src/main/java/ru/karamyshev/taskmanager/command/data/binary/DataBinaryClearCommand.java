package ru.karamyshev.taskmanager.command.data.binary;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.command.AbstractDataCommand;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;

import java.io.Serializable;

public class DataBinaryClearCommand extends AbstractDataCommand implements Serializable {

    @NotNull
    @Override
    public String arg() {
        return "data-bin-clear";
    }

    @NotNull
    @Override
    public String name() {
        return "-dtbnclr";
    }

    @Override
    public @NotNull String description() {
        return "Remove binary file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[REMOVE BINARY FILE]");
        final Session session = serviceLocator.getSessionService().getSession();
        serviceLocator.getAdminEndpoint().clearDataBinary(session);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
