package ru.karamyshev.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.endpoint.Exception_Exception;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.util.TerminalUtil;

public class UserUnlockCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-unlckusr";
    }

    @NotNull
    @Override
    public String name() {
        return "unlock-user";
    }

    @Override
    public @NotNull String description() {
        return "unlocked users";
    }

    @Override
    public void execute() throws Exception_Exception {
        System.out.println("[UNLOCK USER]");
        System.out.println("[ENTER LOGIN]");
        final String login = TerminalUtil.nextLine();
        final Session session = serviceLocator.getSessionService().getSession();
        serviceLocator.getUserEndpoint().unlockUserByLogin(session, login);

        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{ Role.ADMIN};
    }

}
