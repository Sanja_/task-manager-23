package ru.karamyshev.taskmanager.command.task;


import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.endpoint.Task;
import ru.karamyshev.taskmanager.util.TerminalUtil;


public class TaskRemoveByIdCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-tskrmvid";
    }

    @NotNull
    @Override
    public String name() {
        return "task-remove-by-id";
    }

    @Override
    public @NotNull String description() {
        return "Remove task by id.";
    }

    @Override
    public void execute() throws Exception {
        final Session session = serviceLocator.getSessionService().getSession();
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER TASK ID FOR DELETION:");
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskEndpoint().removeTaskOneById(session, id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @NotNull
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
