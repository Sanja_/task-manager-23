package ru.karamyshev.taskmanager.command.data.binary;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.command.AbstractDataCommand;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;

public class DataBinarySaveCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String arg() {
        return "-dtbnsv";
    }

    @NotNull
    @Override
    public String name() {
        return "data-bin-save";
    }

    @Override
    public @NotNull String description() {
        return "Save data from binary file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA BINARY SAVE]");
        final Session session = serviceLocator.getSessionService().getSession();
        serviceLocator.getAdminEndpoint().saveDataBinary(session);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
