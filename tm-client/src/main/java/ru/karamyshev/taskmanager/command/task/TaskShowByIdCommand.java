package ru.karamyshev.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.endpoint.Task;
import ru.karamyshev.taskmanager.util.TerminalUtil;


public class TaskShowByIdCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-tskvid";
    }

    @NotNull
    @Override
    public String name() {
        return "task-view-by-id";
    }

    @Override
    public @NotNull String description() {
        return "Show task by id.";
    }

    @Override
    public void execute() throws Exception {
        final Session session = serviceLocator.getSessionService().getSession();
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER TASK-ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskEndpoint().findTaskOneById(session, id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    private void showTask(final Task task) {
        if (task == null) return;
        System.out.println("\n");
        System.out.println("ID:" + task.getId());
        System.out.println("NAME:" + task.getName());
        System.out.println("DESCRIPTION:" + task.getDescription());
    }

    @NotNull
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
