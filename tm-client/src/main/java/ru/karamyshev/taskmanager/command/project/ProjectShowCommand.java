package ru.karamyshev.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.endpoint.Exception_Exception;
import ru.karamyshev.taskmanager.endpoint.Project;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;

import java.util.List;

public class ProjectShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-prlst";
    }

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @Override
    public @NotNull String description() {
        return "Show project list.";
    }

    @Override
    public void execute() throws Exception_Exception {
        final Session session = serviceLocator.getSessionService().getSession();
        System.out.println("[PROJECT LIST]");
        final List<Project> projects = serviceLocator.getProjectEndpoint().findAllProject(session);
        int index = 1;
        for (Project project : projects) {
            System.out.println(index + ". " + project.getName());
            index++;
        }
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
