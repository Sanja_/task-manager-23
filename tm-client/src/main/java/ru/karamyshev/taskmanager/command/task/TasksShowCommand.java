package ru.karamyshev.taskmanager.command.task;


import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.endpoint.Task;
import java.util.List;

public class TasksShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-tsklst";
    }

    @NotNull
    @Override
    public String name() {
        return "task-list";
    }

    @Override
    public @NotNull String description() {
        return "Show task list.";
    }

    @Override
    public void execute() throws Exception {
        final Session session = serviceLocator.getSessionService().getSession();
        System.out.println("[LIST TASKS]");
        final List<Task> tasks = serviceLocator.getTaskEndpoint().findAllTask(session);
        int index = 1;
        for (Task task: tasks) {
            System.out.println(index + ". " + task.getName());
            index++;
        }
        System.out.println("[OK]");
    }

    @NotNull
    public Role[] roles(){
        return new Role[] { Role.USER, Role.ADMIN };
    }

}
