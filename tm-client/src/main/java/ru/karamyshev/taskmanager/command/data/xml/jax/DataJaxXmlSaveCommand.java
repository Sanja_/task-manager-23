package ru.karamyshev.taskmanager.command.data.xml.jax;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.command.AbstractDataCommand;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;

public class DataJaxXmlSaveCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-jax-xml-save";
    }

    @Override
    public @NotNull String description() {
        return "Save data from xml file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA XML(JAX-B) SAVE]");
        final Session session = serviceLocator.getSessionService().getSession();
        serviceLocator.getAdminEndpoint().saveDataJaxBXml(session);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
