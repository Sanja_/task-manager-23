package ru.karamyshev.taskmanager.endpoint;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.karamyshev.taskmanager.api.endpoint.IProjectEndpoint;
import ru.karamyshev.taskmanager.api.service.IServiceLocator;
import ru.karamyshev.taskmanager.bootstrap.Bootstrap;
import ru.karamyshev.taskmanager.entity.Project;
import ru.karamyshev.taskmanager.entity.Session;
import ru.karamyshev.taskmanager.entity.User;

import java.util.List;

public class ProjectEndpointTest {

    private final static IServiceLocator serviceLocator = new Bootstrap();

    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(serviceLocator);

    @BeforeClass
    public static void init() {
        serviceLocator.getUserService().create("user-1", "user-1");
    }

    @Test
    public void createNameProjectTest() throws Exception {
        final IServiceLocator serviceLocator = new Bootstrap();
        serviceLocator.getUserService().create("user-2", "user-2");
        final IProjectEndpoint projectEndpoint = new ProjectEndpoint(serviceLocator);
        final Session session = serviceLocator.getSessionService().open("user-2", "user-2");
        Assert.assertNotNull(session);
        projectEndpoint.createNameProject(session, "project-1");
        projectEndpoint.createNameProject(session, "project-2");
        Assert.assertEquals(projectEndpoint.getProjectList(session).size(), 2);
    }

    @Test
    public void createDescriptionProjectTest() throws Exception {
        final IProjectEndpoint projectEndpoint = new ProjectEndpoint(serviceLocator);
        final Session session = serviceLocator.getSessionService().open("user-1", "user-1");
        Assert.assertNotNull(session);
        projectEndpoint.createDescriptionProject(session, "project-1", "project-1");
        Assert.assertEquals(projectEndpoint.getProjectList(session).size(), 1);
    }

    @Test
    public void addProjectTest() throws Exception {
        final IServiceLocator serviceLocator = new Bootstrap();
        serviceLocator.getUserService().create("user-1", "user-1");
        final IProjectEndpoint projectEndpoint = new ProjectEndpoint(serviceLocator);
        final Session session = serviceLocator.getSessionService().open("user-1", "user-1");
        Assert.assertNotNull(session);
        final Project project = new Project();
        project.setName("project-1");
        project.setId(111111L);
        projectEndpoint.addProject(session, project);
        Assert.assertEquals(projectEndpoint.getProjectList(session).size(), 1);
    }

    @Test
    public void getProjectListTest() throws Exception {
        final IServiceLocator serviceLocator = new Bootstrap();
        serviceLocator.getUserService().create("user-1", "user-1");
        final IProjectEndpoint projectEndpoint = new ProjectEndpoint(serviceLocator);
        final Session session = serviceLocator.getSessionService().open("user-1", "user-1");
        projectEndpoint.createNameProject(session, "project-1");
        projectEndpoint.createNameProject(session, "project-2");
        projectEndpoint.createNameProject(session, "project-3");
        Assert.assertNotNull(session);
        Assert.assertEquals(projectEndpoint.getProjectList(session).size(), 3);
    }

    @Test
    public void findAllProjectTest() throws Exception {
        final IProjectEndpoint projectEndpoint = new ProjectEndpoint(serviceLocator);
        final Session session = serviceLocator.getSessionService().open("user-1", "user-1");
        projectEndpoint.createNameProject(session, "project-1");
        projectEndpoint.createNameProject(session, "project-2");
        projectEndpoint.createNameProject(session, "project-3");
        Assert.assertEquals(projectEndpoint.findAllProject(session).size(), 4);
    }

    @Test
    public void findOneProjectByIndexTest() throws Exception {
        final IProjectEndpoint projectEndpoint = new ProjectEndpoint(serviceLocator);
        final Session session = serviceLocator.getSessionService().open("user-1", "user-1");
        projectEndpoint.createNameProject(session, "project-1");
        final Project project = projectEndpoint.findOneProjectByIndex(session, 1);
        Assert.assertNotNull(project);
    }

    @Test
    public void findOneProjectByNameTest() throws Exception {
        final IServiceLocator serviceLocator = new Bootstrap();
        serviceLocator.getUserService().create("user-1", "user-1");
        final IProjectEndpoint projectEndpoint = new ProjectEndpoint(serviceLocator);
        final Session session = serviceLocator.getSessionService().open("user-1", "user-1");
        projectEndpoint.createNameProject(session, "project-1");
        final List<Project> project = projectEndpoint.findOneProjectByName(session, "project-1");
        Assert.assertNotNull(project);
        Assert.assertEquals(1, project.size());
    }

    @Test
    public void findOneProjectByIdTest() throws Exception {
        final IProjectEndpoint projectEndpoint = new ProjectEndpoint(serviceLocator);
        final Session session = serviceLocator.getSessionService().open("user-1", "user-1");
        projectEndpoint.createNameProject(session, "project-1");
        final Project project = projectEndpoint.findOneProjectByIndex(session, 1);
        final Project projectTemp = projectEndpoint.findOneProjectById(session, String.valueOf(project.getId()));
        Assert.assertNotNull(projectTemp);
    }


    @Test
    public void clearProjectTest() throws Exception {
        final IServiceLocator serviceLocator = new Bootstrap();
        serviceLocator.getUserService().create("user-1", "user-1");
        final IProjectEndpoint projectEndpoint = new ProjectEndpoint(serviceLocator);
        final Session session = serviceLocator.getSessionService().open("user-1", "user-1");
        projectEndpoint.createNameProject(session, "project-1");
        projectEndpoint.createNameProject(session, "project-2");
        projectEndpoint.createNameProject(session, "project-3");
        Assert.assertEquals(projectEndpoint.findAllProject(session).size(), 3);
        projectEndpoint.clearProject(session);
        Assert.assertEquals(projectEndpoint.findAllProject(session).size(), 0);
    }

    @Test
    public void updateProjectByIdTest() throws Exception {
        final IProjectEndpoint projectEndpoint = new ProjectEndpoint(serviceLocator);
        final Session session = serviceLocator.getSessionService().open("user-1", "user-1");
        projectEndpoint.createNameProject(session, "project-1");
        final Project project = serviceLocator.getProjectService().findOneByIndex(session.getUserId(), 1);
        Assert.assertNotNull(project);
        final Project projectUpdate = projectEndpoint.updateProjectById(session, String.valueOf(project.getId()),
                "user-11", "user-11");
        Assert.assertEquals(projectUpdate.getName(), "user-11");
    }

    @Test
    public void updateProjectByIndexTest() throws Exception {
        final IProjectEndpoint projectEndpoint = new ProjectEndpoint(serviceLocator);
        final Session session = serviceLocator.getSessionService().open("user-1", "user-1");
        projectEndpoint.createNameProject(session, "project-1");
        final Project project = serviceLocator.getProjectService().findOneByIndex(session.getUserId(), 1);
        Assert.assertNotNull(project);
        final Project projectUpdate = projectEndpoint.updateProjectByIndex(session, 1,
                "user-11", "user-11");
        Assert.assertNotNull(projectUpdate);
        Assert.assertEquals(projectUpdate.getName(), "user-11");
    }

    @Test
    public void removeProjectTest() throws Exception {
        final IServiceLocator serviceLocator = new Bootstrap();
        serviceLocator.getUserService().create("user-1", "user-1");
        final IProjectEndpoint projectEndpoint = new ProjectEndpoint(serviceLocator);
        final Session session = serviceLocator.getSessionService().open("user-1", "user-1");
        projectEndpoint.createNameProject(session, "project-1");
        projectEndpoint.createNameProject(session, "project-2");
        projectEndpoint.createNameProject(session, "project-3");
        final List<Project> project = projectEndpoint.findOneProjectByName(session, "project-1");
        Assert.assertEquals(projectEndpoint.getProjectList(session).size(), 3);
        Assert.assertEquals(project.size(), 1);
        projectEndpoint.removeProject(session, project.get(0));
        Assert.assertEquals(projectEndpoint.getProjectList(session).size(), 2);
    }

    @Test
    public void removeOneProjectByIndexTest() throws Exception {
        final IServiceLocator serviceLocator = new Bootstrap();
        serviceLocator.getUserService().create("user-1", "user-1");
        final IProjectEndpoint projectEndpoint = new ProjectEndpoint(serviceLocator);
        final Session session = serviceLocator.getSessionService().open("user-1", "user-1");
        projectEndpoint.createNameProject(session, "project-1");
        projectEndpoint.createNameProject(session, "project-2");
        projectEndpoint.createNameProject(session, "project-3");
        Assert.assertEquals(projectEndpoint.getProjectList(session).size(), 3);
        projectEndpoint.removeOneProjectByIndex(session, 1);
        Assert.assertEquals(projectEndpoint.getProjectList(session).size(), 2);
    }

    @Test
    public void removeOneProjectByNameTest() throws Exception {
        final IServiceLocator serviceLocator = new Bootstrap();
        serviceLocator.getUserService().create("user-1", "user-1");
        final IProjectEndpoint projectEndpoint = new ProjectEndpoint(serviceLocator);
        final Session session = serviceLocator.getSessionService().open("user-1", "user-1");
        projectEndpoint.createNameProject(session, "project-1");
        projectEndpoint.createNameProject(session, "project-2");
        projectEndpoint.createNameProject(session, "project-3");
        Assert.assertEquals(projectEndpoint.getProjectList(session).size(), 3);
        projectEndpoint.removeOneProjectByName(session, "project-1");
        Assert.assertEquals(projectEndpoint.getProjectList(session).size(), 2);
    }

    @Test
    public void removeOneProjectByIdTest() throws Exception {
        final IServiceLocator serviceLocator = new Bootstrap();
        serviceLocator.getUserService().create("user-1", "user-1");
        final IProjectEndpoint projectEndpoint = new ProjectEndpoint(serviceLocator);
        final Session session = serviceLocator.getSessionService().open("user-1", "user-1");
        projectEndpoint.createNameProject(session, "project-1");
        projectEndpoint.createNameProject(session, "project-2");
        projectEndpoint.createNameProject(session, "project-3");
        final Project project = serviceLocator.getProjectService()
                .findOneByIndex(session.getUserId(), 1);
        Assert.assertNotNull(project);
        Assert.assertEquals(projectEndpoint.getProjectList(session).size(), 3);
        projectEndpoint.removeOneProjectById(session, String.valueOf(project.getId()));
        Assert.assertEquals(projectEndpoint.getProjectList(session).size(), 2);
    }

}
