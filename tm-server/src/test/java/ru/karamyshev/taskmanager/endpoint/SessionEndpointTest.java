package ru.karamyshev.taskmanager.endpoint;

import org.junit.Assert;
import org.junit.Test;
import ru.karamyshev.taskmanager.api.endpoint.ISessionEndpoint;
import ru.karamyshev.taskmanager.api.service.IServiceLocator;
import ru.karamyshev.taskmanager.bootstrap.Bootstrap;
import ru.karamyshev.taskmanager.entity.Session;
import ru.karamyshev.taskmanager.entity.User;

public class SessionEndpointTest {

    final IServiceLocator serviceLocator = new Bootstrap();

    final ISessionEndpoint sessionEndpoint = new SessionEndpoint(serviceLocator);

    @Test
    public void openSessionTest() throws Exception {
        serviceLocator.getUserService().create("user-1", "user-1");
        final User user = serviceLocator.getUserService().findByLogin("user-1");
        Assert.assertNotNull(user);
        final Session session = sessionEndpoint.openSession("user-1", "user-1");
        Assert.assertNotNull(session);
    }

    @Test
    public void closeSessionTest() throws Exception {
        serviceLocator.getUserService().create("user-1", "user-1");
        final User user = serviceLocator.getUserService().findByLogin("user-1");
        Assert.assertNotNull(user);
        final Session session = sessionEndpoint.openSession("user-1", "user-1");
        Assert.assertNotNull(session);
        sessionEndpoint.closeSession(session);
        Assert.assertEquals(0, serviceLocator.getSessionService().getList().size());
    }

    @Test
    public void closeAllSessionTest() throws Exception {
        serviceLocator.getUserService().create("user-1", "user-1");
        serviceLocator.getUserService().create("user-2", "user-2");
        final User user1 = serviceLocator.getUserService().findByLogin("user-1");
        final User user2 = serviceLocator.getUserService().findByLogin("user-1");
        Assert.assertNotNull(user1);
        Assert.assertNotNull(user2);
        final Session session1 = sessionEndpoint.openSession("user-1", "user-1");
        final Session session2 = sessionEndpoint.openSession("user-2", "user-2");
        Assert.assertEquals(2, serviceLocator.getSessionService().getList().size());
        sessionEndpoint.closeAllSession(session1);
        Assert.assertEquals(1, serviceLocator.getSessionService().getList().size());
    }

}
