package ru.karamyshev.taskmanager.endpoint;

import org.junit.Assert;
import org.junit.Test;
import ru.karamyshev.taskmanager.api.endpoint.IUserEndpoint;
import ru.karamyshev.taskmanager.api.service.IServiceLocator;
import ru.karamyshev.taskmanager.bootstrap.Bootstrap;
import ru.karamyshev.taskmanager.entity.Session;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.enumerated.Role;

import java.util.ArrayList;
import java.util.List;

public class UserEndpointTest {

    private final IServiceLocator serviceLocator = new Bootstrap();

    private final IUserEndpoint userEndpoint = new UserEndpoint(serviceLocator);

    @Test
    public void createUserEmailTest() throws Exception {
        userEndpoint.createUserEmail("user-2", "user-2", "user-2@com");
        userEndpoint.createUserEmail("user-3", "user-3", "user-3@com");
        Assert.assertEquals(serviceLocator.getUserService().getList().size(), 2);
        userEndpoint.createUserEmail("user-1", "user-1", "user-1@com");
        Assert.assertEquals(serviceLocator.getUserService().getList().size(), 3);
    }

    @Test
    public void createUserRoleTest() throws Exception {
        serviceLocator.getUserService().create("user-2", "user-2", Role.USER);
        serviceLocator.getUserService().create("user-3", "user-3", Role.USER);
        Assert.assertEquals(serviceLocator.getUserService().getList().size(), 2);
        userEndpoint.createUserRole("user-1", "user-1", Role.ADMIN);
        Assert.assertEquals(serviceLocator.getUserService().getList().size(), 3);
    }

    @Test
    public void findUserByIdTest() throws Exception {
        final User user = serviceLocator.getUserService().create("user-2", "user-2");
        Assert.assertNotNull(user);
        final Session currentSession = serviceLocator.getSessionService()
                .open("user-2", "user-2");
        Assert.assertNotNull(currentSession);
        final User userTemp = userEndpoint.findUserById(currentSession);
        Assert.assertEquals(user.getLogin(), userTemp.getLogin());
    }

    @Test
    public void findUserByLoginTest() throws Exception {
        serviceLocator.getUserService().create("user-1", "user-1");
        final User user = serviceLocator.getUserService().create("user-2", "user-2");
        Assert.assertNotNull(user);
        final Session currentSession = serviceLocator.getSessionService()
                .open("user-1", "user-1");
        Assert.assertNotNull(currentSession);
        final User userTemp = userEndpoint.findUserByLogin(currentSession, "user-2");
        Assert.assertEquals(user.getLogin(), userTemp.getLogin());
    }

    @Test
    public void removeUserTest() throws Exception {
        final User admin = serviceLocator.getUserService().create("user-1", "user-1");
        admin.setRole(Role.ADMIN);
        final User user = serviceLocator.getUserService().create("user-2", "user-2");
        final Session currentSession = serviceLocator.getSessionService()
                .open("user-1", "user-1");
        userEndpoint.removeUser(currentSession, user);
        final User tempUser = userEndpoint.findUserByLogin(currentSession, user.getLogin());
        Assert.assertNull(tempUser);
    }

    @Test
    public void removeUserByIdTest() throws Exception {
        serviceLocator.getUserService().create("user-2", "user-2");
        final Session currentSession = serviceLocator.getSessionService()
                .open("user-2", "user-2");
        userEndpoint.removeUserById(currentSession);
        final User tempUser = userEndpoint.findUserById(currentSession);
        Assert.assertNull(tempUser);
    }

    @Test
    public void removeUserByLoginTest() throws Exception {
        final User admin = serviceLocator.getUserService().create("user-1", "user-1");
        admin.setRole(Role.ADMIN);
        final User user = serviceLocator.getUserService().create("user-2", "user-2");
        final Session currentSession = serviceLocator.getSessionService()
                .open("user-1", "user-1");
        userEndpoint.removeByLogin(currentSession, user.getLogin());
        final User tempUser = userEndpoint.findUserByLogin(currentSession, user.getLogin());
        Assert.assertNull(tempUser);
    }

    @Test
    public void lockUserByLoginTest() throws Exception {
        final User admin = serviceLocator.getUserService().create("user-1", "user-1");
        admin.setRole(Role.ADMIN);
        final User user = serviceLocator.getUserService().create("user-2", "user-2");
        final Session currentSession = serviceLocator.getSessionService()
                .open("user-1", "user-1");
        userEndpoint.lockUserByLogin(currentSession, admin.getLogin(), user.getLogin());
        Assert.assertTrue(user.getLocked());
    }

    @Test
    public void unlockUserByLoginTest() throws Exception {
        final User admin = serviceLocator.getUserService().create("user-1", "user-1");
        admin.setRole(Role.ADMIN);
        final User user = serviceLocator.getUserService().create("user-2", "user-2");
        user.setLocked(true);
        final Session currentSession = serviceLocator.getSessionService()
                .open("user-1", "user-1");
        userEndpoint.unlockUserByLogin(currentSession, user.getLogin());
        Assert.assertFalse(user.getLocked());
    }

    @Test
    public void getUserListTest() throws Exception {
        serviceLocator.getUserService().create("user-1", "user-1");
        final Session session = serviceLocator.getSessionService()
                .open("user-1", "user-1");
        Assert.assertNotNull(session);
        Assert.assertEquals(userEndpoint.getUserList(session).size(), 1);

        serviceLocator.getUserService().create("user-2", "user-2");
        serviceLocator.getUserService().create("user-3", "user-3");
        Assert.assertEquals(userEndpoint.getUserList(session).size(), 3);
    }

    @Test
    public void loadUserTest() throws Exception {
        final List<User> userList = new ArrayList<>();
        final User user1 = new User();
        final User user2 = new User();
        user1.setLogin("user-1");
        user1.setPasswordHash("user-11111111");
        user2.setLogin("user-2");
        user2.setPasswordHash("user-22222222");
        userList.add(user1);
        userList.add(user2);

        userEndpoint.createUser("admin", "admin");
        final Session currentSession = serviceLocator.getSessionService()
                .open("admin", "admin");
        Assert.assertNotNull(currentSession);
        Assert.assertEquals(userEndpoint.getUserList(currentSession).size(), 1);
        userEndpoint.loadUser(userList, currentSession);
        Assert.assertEquals(userEndpoint.getUserList(currentSession).size(), 2);
        serviceLocator.getSessionService().close(currentSession);
    }

}
