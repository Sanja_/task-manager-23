package ru.karamyshev.taskmanager.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.karamyshev.taskmanager.api.repository.ISessionRepository;
import ru.karamyshev.taskmanager.api.service.IServiceLocator;
import ru.karamyshev.taskmanager.api.service.ISessionService;
import ru.karamyshev.taskmanager.bootstrap.Bootstrap;
import ru.karamyshev.taskmanager.entity.Session;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.enumerated.Role;
import ru.karamyshev.taskmanager.exception.user.AccessDeniedException;
import ru.karamyshev.taskmanager.repository.SessionRepository;

import java.util.List;

public class SessionServiceTest {

    private final IServiceLocator serviceLocator = new Bootstrap();

    private final ISessionRepository sessionRepository = new SessionRepository();

    private final ISessionService sessionService = new SessionService(sessionRepository, serviceLocator);

    private final static Session session1 = new Session();

    private final static Session session2 = new Session();

    private final static Session session3 = new Session();

    private static User user1 = new User();

    @Before
    public void createUserTest() {
        user1 = serviceLocator.getUserService().create("user", "user", "user@com");
        user1.setId(2111111111L);
        user1.setRole(Role.USER);
    }

    @Test(expected = AccessDeniedException.class)
    public void closeTest() throws Exception {
        final Session session = sessionService.open("test", "test");
        final List<Session> sessionList = sessionService.getListSession(session);
        Assert.assertEquals(sessionList.size(), 1);
        sessionService.close(session);
        Assert.assertEquals(0, sessionService.getList().size());
    }

    @Test
    public void closeAllTest() throws Exception {
        serviceLocator.getUserService().create("test", "test");
        sessionService.open("user", "user");
        final Session session = sessionService.open("test", "test");
        sessionService.open("test", "test");
        Assert.assertNotNull(session);
        final List<Session> sessionList = sessionService.getListSession(session);
        Assert.assertEquals(sessionList.size(), 2);
        sessionService.closeAll(session);
        Assert.assertEquals(1, sessionService.getList().size());
    }

    @Test
    public void openTest() throws Exception {
        final Session session = sessionService.open("user", "user");
        Assert.assertNotNull(session);
        Assert.assertEquals(session, sessionRepository.findByUserId(session.getUserId()));
    }


    @Test
    public void getUserTest() throws Exception {
        final Session session = sessionService.open("user", "user");
        Assert.assertNotNull(session);
        final User user = sessionService.getUser(session);
        Assert.assertEquals(String.valueOf(user.getId()), session.getUserId());
    }

    @Test(expected = AccessDeniedException.class)
    public void getListSessionTest() throws Exception {
        sessionService.open("test", "test");
        sessionService.open("test", "test");
        sessionService.open("test", "test");
        sessionService.open("test", "test");
        final Session session = sessionService.open("test", "test");
        final List<Session> sessionList = sessionService.getListSession(session);
        Assert.assertEquals(sessionList.size(), 5);
    }

    @Test
    public void signTest() {
        session1.setUserId("1111111111");
        final Session session = sessionService.sign(session1);
        Assert.assertEquals(session.getUserId(), session1.getUserId());
    }

    @Test
    public void isValidTest() throws Exception {
        final Session session = sessionService.open("user", "user");
        Assert.assertNotNull(session);
        Assert.assertTrue(sessionService.isValid(session));
    }

    @Test
    public void validateTest() throws Exception {
        final Session session = sessionService.open(user1.getLogin(), "user");
        Assert.assertNotNull(session);
        sessionService.validate(session, user1.getRole());
    }

    @Test
    public void checkDataAccessTest() {
        boolean valid = sessionService.checkDataAccess(user1.getLogin(), "user");
        Assert.assertTrue(valid);
    }

    @Test(expected = AccessDeniedException.class)
    public void signOutByLoginTest() throws Exception {
        sessionService.signOutByLogin(user1.getLogin() + "a");

    }

    @Test
    public void signOutByUserIdTest() throws Exception {
        final User user = serviceLocator.getUserService().create("test", "test");
        sessionService.open("test", "test");
        Assert.assertNotNull(user);
        sessionService.signOutByUserId(String.valueOf(user.getId()));
        Assert.assertEquals(sessionService.getList().size(), 0);
    }

}
