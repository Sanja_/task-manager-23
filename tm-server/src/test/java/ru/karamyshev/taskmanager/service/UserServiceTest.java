package ru.karamyshev.taskmanager.service;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.karamyshev.taskmanager.api.repository.IUserRepository;
import ru.karamyshev.taskmanager.api.service.IUserService;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.enumerated.Role;
import ru.karamyshev.taskmanager.repository.UserRepository;

import java.util.List;

public class UserServiceTest {

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final static User user = new User();

    @BeforeClass
    public static void init() {
        user.setLogin("user");
        user.setId(111);
        user.setRole(Role.USER);
        user.setEmail("user@com");
    }

    @Test
    public void createTest() {
        final List<User> userList = userService.getList();
        Assert.assertEquals(userList.size(), 0);
        userService.create("user1", "user1");
        Assert.assertEquals(userService.getList().size(), 1);
    }

    @Test
    public void createUserEmailTest() {
        final List<User> userList = userService.getList();
        Assert.assertEquals(userList.size(), 0);
        userService.create("user1", "user1", "user1@.com");
        Assert.assertEquals(userService.getList().size(), 1);
    }

    @Test
    public void createUserRoleTest() {
        final List<User> userList = userService.getList();
        Assert.assertEquals(userList.size(), 0);
        userService.create("user1", "user1", Role.USER);
        Assert.assertEquals(userService.getList().size(), 1);
    }

    @Test
    public void findByIdTest() throws Exception {
        final User user = userService.create("user-1", "user-1", "user@com");
        final User tempUser = userService.findById(String.valueOf(user.getId()));
        Assert.assertNotNull(tempUser);
        Assert.assertEquals(user, tempUser);
    }

    @Test
    public void findByLoginTest() throws Exception {
        final User user = userService.create("user-1", "user-1", "user@com");
        final User user2 = userService.create("user-2", "user-2", "user2@com");
        final User tempUser = userService.findByLogin(user.getLogin());
        Assert.assertNotNull(tempUser);
        Assert.assertEquals(user, tempUser);
    }

    @Test
    public void removeByLoginTest() throws Exception {
        final User user1 = userService.create("user-1", "user-1", "user1@com");
        final User user2 = userService.create("user-2", "user-2", "user2@com");
        Assert.assertEquals(userService.getList().size(), 2);
        userService.removeByLogin(user2.getLogin());
        Assert.assertEquals(userService.getList().size(), 1);
    }

    @Test
    public void removeByIdTest() throws Exception {
        final User user1 = userService.create("user-1", "user-1", "user1@com");
        final User user2 = userService.create("user-2", "user-2", "user2@com");
        final List<User> userList = userService.getList();
        Assert.assertEquals(userList.size(), 2);
        userService.removeById(String.valueOf(user2.getId()));
        Assert.assertEquals(userService.getList().size(), 1);
    }

    @Test
    public void removeUserTest() throws Exception {
        final User user1 = userService.create("user-1", "user-1", "user1@com");
        final User user2 = userService.create("user-2", "user-2", "user2@com");
        final List<User> userList = userService.getList();
        Assert.assertEquals(userList.size(), 2);
        userService.removeUser(user2);
        Assert.assertEquals(userService.getList().size(), 1);
    }

    @Test
    public void removeUserByLoginTest() throws Exception {
        final User user1 = userService.create("user-1", "user-1", "user1@com");
        final User user2 = userService.create("user-2", "user-2", "user2@com");
        final List<User> userList = userService.getList();
        Assert.assertEquals(userList.size(), 2);
        userService.removeUserByLogin("user-2", "user-1");
        Assert.assertEquals(userService.getList().size(), 1);
    }

    @Test
    public void lockUserByLoginTest() {
        final User user1 = userService.create("user-1", "user-1", "user1@com");
        Assert.assertNotNull(user1);
        final User userLock = userService.lockUserByLogin(user.getLogin(), user1.getLogin());
        Assert.assertEquals(userLock.getLocked(), true);
    }

    @Test
    public void unlockUserByLoginTest() {
        final User user1 = userService.create("user-1", "user-1", "user1@com");
        Assert.assertNotNull(user1);
        final User userLock = userService.lockUserByLogin(user.getLogin(), user1.getLogin());
        Assert.assertEquals(user1.getLocked(), true);
        userService.unlockUserByLogin(userLock.getLogin());
        Assert.assertEquals(userLock.getLocked(), false);
    }

}
