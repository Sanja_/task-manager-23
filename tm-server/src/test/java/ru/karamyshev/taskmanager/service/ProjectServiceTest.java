package ru.karamyshev.taskmanager.service;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;
import ru.karamyshev.taskmanager.api.repository.IProjectRepository;
import ru.karamyshev.taskmanager.api.service.IProjectService;
import ru.karamyshev.taskmanager.entity.Project;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.enumerated.Role;
import ru.karamyshev.taskmanager.repository.ProjectRepository;

import java.util.List;

public class ProjectServiceTest {

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private static final User user = new User();

    @AfterClass
    public static void init() {
        user.setId(1111L);
        user.setLogin("user1");
        user.setRole(Role.USER);
    }

    @Test
    public void createTest() throws Exception {
        final List<Project> projectList = projectRepository.findAll();
        Assert.assertEquals(projectList.size(), 0);
        final String userId = String.valueOf(user.getId());
        projectService.create(userId, "project-1");
        Assert.assertEquals(projectRepository.findAll().size(), 1);
    }

    @Test
    public void createProjectDescriptionTest() throws Exception {
        final List<Project> projectList = projectRepository.findAll();
        Assert.assertEquals(projectList.size(), 0);
        final String userId = String.valueOf(user.getId());
        projectService.create(userId, "project-1", "project-1");
        Assert.assertEquals(projectRepository.findAll().size(), 1);
    }

    @Test
    public void addTest() {
        final Project project = new Project();
        project.setId(111L);
        project.setName("project-1");
        final String userId = String.valueOf(user.getId());
        projectService.add(userId, project);
        Assert.assertEquals(projectRepository.getList().size(), 1);
    }

    @Test
    public void removeTest() throws Exception {
        final String userId = String.valueOf(user.getId());
        projectService.create(userId, "project-1");
        Assert.assertEquals(projectRepository.getList().size(), 1);
        final Project project = projectRepository.findOneByIndex(userId, 0);
        projectService.remove(userId, project);
        Assert.assertEquals(projectRepository.getList().size(), 0);
    }

    @Test
    public void findAll() throws Exception {
        final String userId = String.valueOf(user.getId());
        projectService.create(userId, "project-1");
        projectService.create(userId, "project-1");
        projectService.create(userId, "project-1");
        Assert.assertEquals(projectRepository.getList().size(), 3);
        final List<Project> projectList = projectService.findAll(userId);
        Assert.assertEquals(projectList.size(), 3);
    }

    @Test
    public void clearTest() {
        final String userId = String.valueOf(user.getId());
        projectService.create(userId, "project-1");
        projectService.create(userId, "project-1");
        projectService.create(userId, "project-1");
        Assert.assertEquals(projectRepository.getList().size(), 3);
        projectService.clear(userId);
        Assert.assertEquals(projectRepository.getList().size(), 0);
    }

    @Test
    public void findOneByIndexTest() throws Exception {
        final String userId = String.valueOf(user.getId());
        final Project project = new Project();
        project.setName("project-1");
        project.setId(111);
        projectService.add(userId, project);
        final Project tempTask = projectService.findOneByIndex(userId, 1);
        Assert.assertEquals(project, tempTask);
    }

    @Test
    public void findOneByNameTest() throws Exception {
        final String userId = String.valueOf(user.getId());
        final Project project = new Project();
        project.setName("project-1");
        projectService.add(userId, project);
        final List<Project> tempProject = projectService.findOneByName(userId, project.getName());
        for (Project project1 : tempProject) Assert.assertEquals(project.getName(), project1.getName());
    }

    @Test
    public void removeOneByIndexTest() {
        final String userId = String.valueOf(user.getId());
        projectService.create(userId, "project-1");
        projectService.create(userId, "project-2");
        projectService.create(userId, "project-3");
        Assert.assertEquals(projectService.getList().size(), 3);
        projectService.removeOneByIndex(userId, 1);
        Assert.assertEquals(projectService.getList().size(), 2);
    }

    @Test
    public void removeOneByNameTest() {
        final String userId = String.valueOf(user.getId());
        projectService.create(userId, "project-1");
        projectService.create(userId, "project-2");
        projectService.create(userId, "project-3");
        Assert.assertEquals(projectService.getList().size(), 3);
        projectService.removeOneByName(userId, "project-2");
        Assert.assertEquals(projectService.getList().size(), 2);
    }

    @Test
    public void removeOneByIdTest() {
        final String userId = String.valueOf(user.getId());
        final Project project1 = new Project();
        final Project project2 = new Project();
        final Project project3 = new Project();
        project1.setName("project-1");
        project1.setId(111);
        project2.setName("project-2");
        project2.setId(222);
        project3.setName("project-3");
        project3.setId(333);
        projectService.add(userId, project1);
        projectService.add(userId, project2);
        projectService.add(userId, project3);

        Assert.assertEquals(projectService.getList().size(), 3);
        projectService.removeOneById(userId, String.valueOf(project3.getId()));
        Assert.assertEquals(projectService.getList().size(), 2);
    }

    @Test
    public void updateProjectByIndexTest() {
        final String userId = String.valueOf(user.getId());
        final Project project = new Project();
        project.setName("project-1");
        project.setId(111);
        project.setDescription("111");
        projectService.add(userId, project);
        projectService.updateProjectByIndex(userId, 1,
                "project-2", "222");
        final Project tempProject = projectService.findOneById(userId, "111");
        Assert.assertNotNull(tempProject);
        Assert.assertEquals(tempProject.getId(), 111);
        Assert.assertEquals(tempProject.getName(), "project-2");
        Assert.assertEquals(tempProject.getDescription(), "222");
    }

    @Test
    public void updateProjectByIdTest() {
        final String userId = String.valueOf(user.getId());
        final Project project = new Project();
        project.setName("project-1");
        project.setId(111);
        project.setDescription("111");
        projectService.add(userId, project);
        projectService.updateProjectById(userId, String.valueOf(project.getId()),
                "project-2", "222");
        final Project tempProject = projectService.findOneById(userId, "111");
        Assert.assertNotNull(tempProject);
        Assert.assertEquals(tempProject.getId(), 111);
        Assert.assertEquals(tempProject.getName(), "project-2");
        Assert.assertEquals(tempProject.getDescription(), "222");
    }

}
