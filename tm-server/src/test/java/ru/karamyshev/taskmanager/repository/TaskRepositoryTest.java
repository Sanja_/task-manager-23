package ru.karamyshev.taskmanager.repository;

import org.junit.*;
import ru.karamyshev.taskmanager.api.repository.ITaskRepository;
import ru.karamyshev.taskmanager.entity.Task;

import java.util.List;

public class TaskRepositoryTest {

    private final ITaskRepository taskRepository = new TaskRepository();

    private final static Task task1 = new Task();

    private final static Task task2 = new Task();

    private final static Task task3 = new Task();

    @BeforeClass
    public static void init() {
        task1.setName("Task-test-1");
        task2.setName("Task-test-2");
        task3.setName("Task-test-3");
    }

    @Before
    public void addProjectTest() {
        taskRepository.add("1111111111", task1);
        taskRepository.add("1111111111", task2);
        taskRepository.add("1111111111", task3);
    }

    @After
    public void clearTest() {
        taskRepository.clear();
    }

    @Test
    public void findAllTest() {
        final List<Task> result = taskRepository.findAll(task1.getUserId());
        Assert.assertNotNull(result);
        Assert.assertEquals(taskRepository.findAll(task1.getUserId()).size(), 3);
    }


    @Test
    public void findOneByIdTest() {
        final String projectId = String.valueOf(task1.getId());
        final Task result = taskRepository.findOneById(task1.getUserId(), projectId);
        Assert.assertNotNull(result);
        Assert.assertEquals(result.getName(), task1.getName());
    }

    @Test
    public void removeOneByIdTest() throws Exception {
        taskRepository.findAll(task1.getUserId());
        final String projectId = String.valueOf(task1.getId());
        taskRepository.removeOneById(task1.getUserId(), projectId);
        Assert.assertEquals(taskRepository.findAll(task1.getUserId()).size(), 2);
    }

    @Test
    public void findOneByIndexTest() {
        final Task result = taskRepository.findOneByIndex(task1.getUserId(), 0);
        Assert.assertNotNull(result);
        Assert.assertEquals(result.getName(), task1.getName());
    }

    @Test
    public void findOneByNameTest() {
        final List<Task> result = taskRepository.findOneByName(task1.getUserId(), task1.getName());
        Assert.assertNotNull(result);
        Assert.assertEquals(result.size(), 1);
        Assert.assertEquals(result.get(0).getName(), task1.getName());
    }

    @Test
    public void removeOneByIndexTest() {
        final List<Task> result = taskRepository.findAll(task1.getUserId());
        taskRepository.removeOneByIndex(task1.getUserId(), 0);
        Assert.assertNotNull(result);
        Assert.assertEquals(taskRepository.findAll(task1.getUserId()).size(), 2);
    }

    @Test
    public void removeOneByNameTest() {
        taskRepository.findAll(task1.getUserId());
        taskRepository.removeOneByName(task1.getUserId(), task1.getName());
        taskRepository.removeOneByName(task2.getUserId(), task2.getName());
        Assert.assertEquals(taskRepository.findAll(task1.getUserId()).size(), 1);
    }

}
