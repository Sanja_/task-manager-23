package ru.karamyshev.taskmanager.repository;

import org.junit.*;
import ru.karamyshev.taskmanager.api.repository.ISessionRepository;
import ru.karamyshev.taskmanager.entity.Session;

import java.util.List;

public class SessionRepositoryTest {

    private final ISessionRepository sessionRepository = new SessionRepository();

    private final static Session session1 = new Session();

    private final static Session session2 = new Session();

    private final static Session session3 = new Session();

    @BeforeClass
    public static void init() {
        session1.setSignature("1111111111s");
        session1.setTimestamp(1111111111L);

        session2.setSignature("2222222222s");
        session2.setTimestamp(2111111111L);

        session3.setSignature("3333333333s");
        session3.setTimestamp(3111111111L);
    }

    @Before
    public void addTest() {
        sessionRepository.add("2222222222", session1);
        sessionRepository.add("3333333333", session2);
        sessionRepository.add("1111111111", session3);
        Assert.assertNotNull(sessionRepository.getList());
        Assert.assertEquals(sessionRepository.getList().size(), 3);

        Assert.assertEquals(sessionRepository.getList().get(0).getUserId(), session1.getUserId());
        Assert.assertEquals(sessionRepository.getList().get(1).getUserId(), session2.getUserId());
        Assert.assertEquals(sessionRepository.getList().get(2).getUserId(), session3.getUserId());
    }

    @After
    public void clearTest() {
        sessionRepository.clear();
    }

    @Test
    public void findAllUserSessionTest() {
        final List<Session> sessionList = sessionRepository.findAllSession(session1.getUserId());
        Assert.assertNotNull(sessionList);
        Assert.assertEquals(sessionList.size(), 1);
    }

    @Test
    public void findByUserIdTest() throws Exception {
        final Session session = sessionRepository.findByUserId(session1.getUserId());
        Assert.assertNotNull(session);
        Assert.assertEquals(session.getUserId(), session1.getUserId());
    }

    @Test
    public void findByIdTest() throws Exception {
        final Session session = sessionRepository.findById(String.valueOf(session1.getUserId()));
        Assert.assertNotNull(session);
        Assert.assertEquals(session.getUserId(), session1.getUserId());
    }

    @Test
    public void removeByUserIdTest() throws Exception {
        sessionRepository.removeByUserId(session1.getUserId());
        Assert.assertEquals(sessionRepository.findAll().size(), 2);
    }

}
