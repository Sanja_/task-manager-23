package ru.karamyshev.taskmanager.bootstrap;

import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.api.endpoint.*;
import ru.karamyshev.taskmanager.api.repository.IProjectRepository;
import ru.karamyshev.taskmanager.api.repository.ISessionRepository;
import ru.karamyshev.taskmanager.api.repository.ITaskRepository;
import ru.karamyshev.taskmanager.api.repository.IUserRepository;
import ru.karamyshev.taskmanager.api.service.*;
import ru.karamyshev.taskmanager.endpoint.*;
import ru.karamyshev.taskmanager.enumerated.Role;
import ru.karamyshev.taskmanager.repository.ProjectRepository;
import ru.karamyshev.taskmanager.repository.SessionRepository;
import ru.karamyshev.taskmanager.repository.TaskRepository;
import ru.karamyshev.taskmanager.repository.UserRepository;
import ru.karamyshev.taskmanager.service.*;

import javax.xml.ws.Endpoint;

public class Bootstrap implements IServiceLocator {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IAuthenticationService authenticationService = new AuthenticationService(userService);

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IDomainService domainService = new DomainService(userService, taskService, projectService);

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository, this);

    @NotNull
    private final IAdminService adminService = new AdminService(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IAdminEndpoint adminEndpoint = new AdminEndpoint(this);

    @NotNull
    private final IAdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    @NotNull
    private final IAuthenticationEndpoint authenticationEndpoint = new AuthenticationEndpoint(this);

    @NotNull
    private final String url = "http://" + propertyService.getServerHost() + ":" + propertyService.getServerPort();

    @NotNull
    private final String sessionEndpointUrl = url + "/SessionEndpoint?wsdl";

    @NotNull
    private final String taskEndpointUrl = url + "/TaskEndpoint?wsdl";

    @NotNull
    private final String projectEndpointUrl = url + "/ProjectEndpoint?wsdl";

    @NotNull
    private final String userEndpointUrl = url + "/UserEndpoint?wsdl";

    @NotNull
    private final String adminUserEndpointUrl = url + "/AdminUserEndpoint?wsdl";

    @NotNull
    private final String adminDataEndpointUrl = url + "/AdminEndpoint?wsdl";

    @NotNull
    private final String authenticationDataEndpoint = url + "/AuthenticationEndpoint?wsdl";

    private void initEndpoint() {
        Endpoint.publish(authenticationDataEndpoint, authenticationEndpoint);
        Endpoint.publish(taskEndpointUrl, taskEndpoint);
        Endpoint.publish(userEndpointUrl, userEndpoint);
        Endpoint.publish(projectEndpointUrl, projectEndpoint);
        Endpoint.publish(sessionEndpointUrl, sessionEndpoint);
        Endpoint.publish(adminUserEndpointUrl, adminUserEndpoint);
        Endpoint.publish(adminDataEndpointUrl, adminEndpoint);
    }

    private void showEndpoint() {
        System.out.println(sessionEndpointUrl);
        System.out.println(taskEndpointUrl);
        System.out.println(projectEndpointUrl);
        System.out.println(userEndpointUrl);
        System.out.println(adminUserEndpointUrl);
        System.out.println(adminDataEndpointUrl);
        System.out.println(authenticationDataEndpoint);
    }

    private void initProperty() {
        propertyService.init();
    }

    private void initUser() {
        userService.create("test", "test", "test@test.com");
        userService.create("admin", "admin", Role.ADMIN);
    }

    public void run(final String[] args) {
        initProperty();
        initEndpoint();
        initUser();
        System.out.println("*** TASK MANAGER SERVER ***");
        showEndpoint();
    }

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public IAuthenticationService getAuthenticationService() {
        return authenticationService;
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public IDomainService getDomainService() {
        return domainService;
    }

    @NotNull
    @Override
    public ISessionService getSessionService() {
        return sessionService;
    }

    @NotNull
    @Override
    public IPropertyService getPropertyService() {
        return propertyService;
    }

    @NotNull
    @Override
    public IAdminService getAdminService() {
        return adminService;
    }

}
