package ru.karamyshev.taskmanager.repository;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.repository.ISessionRepository;
import ru.karamyshev.taskmanager.entity.Session;

import java.util.ArrayList;
import java.util.List;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @Override
    public void add(final String userId, final Session session) {
        session.setUserId(userId);
        entities.add(session);
    }

    @Override
    public void remove(final String userId, final Session session) {
        if (!userId.equals(session.getUserId())) return;
        entities.remove(session);
    }

    @Override
    public void clear(final String userId) {
        entities.removeAll(findAllSession(userId));
    }

    @Nullable
    @Override
    public List<Session> findAllSession(final String userId) {
        final List<Session> result = new ArrayList<>();
        for (final Session session : entities) {
            if (userId.equals(session.getUserId())) result.add(session);
        }
        return result;
    }

    @Nullable
    @Override
    public Session findByUserId(final String userId) throws Exception {
        for (final Session session : entities) {
            if (userId.equals(session.getUserId())) return session;
        }
        throw new Exception();
    }

    @Nullable
    @Override
    public Session findById(final String id) throws Exception {
        for (final Session session : entities) {
            if (id.equals(session.getUserId())) return session;
        }
        throw new Exception();
    }

    @Nullable
    @Override
    public Session removeByUserId(final String userId) throws Exception {
        final Session session = findByUserId(userId);
        remove(userId, session);
        return session;
    }

    @Override
    public boolean contains(final String id) throws Exception {
        final Session session = findById(id);
        return findAll().contains(session);
    }

}
