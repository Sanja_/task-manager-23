package ru.karamyshev.taskmanager.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.dto.Result;
import ru.karamyshev.taskmanager.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IAdminEndpoint {

    @Nullable
    @WebMethod
    Result saveDataBinary(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    Result loadDataBinary(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    Result clearDataBinary(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    Result saveDataBase64(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    Result loadDataBase64(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    Result clearDataBase64(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    Result saveDataFasterJson(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    Result loadDataFasterJson(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    Result clearDataFasterJson(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    Result saveDataFasterXml(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    Result loadDataFasterXml(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    Result clearDataFasterXml(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    Result saveDataJaxBJson(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    Result loadDataJaxBJson(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    Result clearDataJaxBJson(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    Result saveDataJaxBXml(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    Result loadDataJaxBXml(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    Result clearDataJaxBXml(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

}
