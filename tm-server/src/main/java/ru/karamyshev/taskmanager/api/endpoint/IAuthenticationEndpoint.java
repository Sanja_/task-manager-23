package ru.karamyshev.taskmanager.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.entity.Session;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IAuthenticationEndpoint {

    @WebMethod
    void login(
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password
    );

    @Nullable
    @WebMethod
    String getUserId(
            @Nullable Session session
    ) throws Exception;

    @WebMethod
    void checkRoles(
            @Nullable Session session,
            @Nullable Role[] roles
    ) throws Exception;

    @Nullable
    @WebMethod
    String getCurrentLogin(
            @Nullable Session session
    ) throws Exception;


    @WebMethod
    boolean isAuth();

    @WebMethod
    void registryUser(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    ) throws Exception;

    @WebMethod
    void renameLogin(@Nullable Session session, @Nullable String newLogin) throws Exception;

    @Nullable
    @WebMethod
    User showProfile(@Nullable Session session, @Nullable String login) throws Exception;

    @WebMethod
    void renamePassword(
            @Nullable Session session,
            @Nullable String oldPassword,
            @Nullable String newPassword
    ) throws Exception;

    @WebMethod
    void logout();

}
