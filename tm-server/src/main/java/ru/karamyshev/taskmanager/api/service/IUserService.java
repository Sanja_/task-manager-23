package ru.karamyshev.taskmanager.api.service;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.enumerated.Role;

public interface IUserService extends IService<User> {

    @Nullable
    User create(@Nullable String login, @Nullable String password);

    @Nullable
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @Nullable
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    User findById(@Nullable String id);

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User removeUser(@Nullable User user);

    @Nullable
    User removeById(@Nullable String id);

    @Nullable
    User removeByLogin(@Nullable String login);

    @Nullable
    User lockUserByLogin(@Nullable String currentLogin, @Nullable String login);

    @Nullable
    User unlockUserByLogin(@Nullable String login);

    @Nullable
    User removeUserByLogin(@Nullable String currentLogin, @Nullable String login);

}
