package ru.karamyshev.taskmanager.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.dto.Result;
import ru.karamyshev.taskmanager.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface ISessionEndpoint {

    @Nullable
    @WebMethod
    Session openSession(
            @WebParam(name = "login", partName = "login") String login,
            @WebParam(name = "password", partName = "password") String password
    ) throws Exception;

    @Nullable
    @WebMethod
    Result closeSession(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    Result closeAllSession(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws Exception;

}
