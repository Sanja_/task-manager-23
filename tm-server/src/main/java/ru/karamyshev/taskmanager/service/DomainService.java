package ru.karamyshev.taskmanager.service;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.service.IDomainService;
import ru.karamyshev.taskmanager.api.service.IProjectService;
import ru.karamyshev.taskmanager.api.service.ITaskService;
import ru.karamyshev.taskmanager.api.service.IUserService;
import ru.karamyshev.taskmanager.dto.Domain;

public class DomainService implements IDomainService {

    private final IUserService userService;

    private final ITaskService taskService;

    private final IProjectService projectService;

    public DomainService(
            final IUserService userService,
            final ITaskService taskService,
            final IProjectService projectService
    ) {
        this.userService = userService;
        this.taskService = taskService;
        this.projectService = projectService;
    }

    @Override
    public void load(@Nullable final Domain domain) {
        if (domain == null) return;
        projectService.load(domain.getProjects());
        taskService.load(domain.getTasks());
        userService.load(domain.getUsers());
    }

    @Override
    public void export(@Nullable final Domain domain) {
        if (domain == null) return;
        domain.setProjects(projectService.getList());
        domain.setTasks(taskService.getList());
        domain.setUsers(userService.getList());
    }

}
