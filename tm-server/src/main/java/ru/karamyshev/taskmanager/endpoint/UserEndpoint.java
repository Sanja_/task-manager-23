package ru.karamyshev.taskmanager.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.endpoint.IUserEndpoint;
import ru.karamyshev.taskmanager.api.service.IServiceLocator;
import ru.karamyshev.taskmanager.entity.Session;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class UserEndpoint implements IUserEndpoint {

    private IServiceLocator serviceLocator;

    public UserEndpoint() {
    }

    public UserEndpoint(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    @Override
    @WebMethod
    public User createUser(
            @WebParam(name = "login", partName = "login") @Nullable String login,
            @WebParam(name = "password", partName = "password") @Nullable String password
    ) throws Exception {
        return serviceLocator.getUserService().create(login, password);
    }

    @Nullable
    @Override
    @WebMethod
    public User createUserEmail(
            @WebParam(name = "login", partName = "login") @Nullable String login,
            @WebParam(name = "password", partName = "password") @Nullable String password,
            @WebParam(name = "email", partName = "email") @Nullable String email
    ) throws Exception {
        return serviceLocator.getUserService().create(login, password, email);
    }

    @Nullable
    @Override
    @WebMethod
    public User createUserRole(
            @WebParam(name = "login", partName = "login") @Nullable String login,
            @WebParam(name = "password", partName = "password") @Nullable String password,
            @WebParam(name = "role", partName = "role") @Nullable Role role
    ) throws Exception {
        return serviceLocator.getUserService().create(login, password, role);
    }

    @Nullable
    @Override
    @WebMethod
    public User findUserById(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().findById(session.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public User findUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().findByLogin(login);
    }

    @Nullable
    @Override
    @WebMethod
    public User removeUser(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "user", partName = "user") @Nullable User user
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().removeUser(user);
    }

    @Nullable
    @Override
    @WebMethod
    public User removeUserById(
            @WebParam(name = "session", partName = "session") @Nullable Session session/*,
            @WebParam (name = "id", partName = "id") @Nullable String id*/
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().removeById(session.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public User removeByLogin(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().removeByLogin(login);
    }

    @Nullable
    @Override
    @WebMethod
    public User lockUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "currentLogin", partName = "currentLogin") @Nullable String currentLogin,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().lockUserByLogin(currentLogin, login);
    }

    @Nullable
    @Override
    @WebMethod
    public User unlockUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().unlockUserByLogin(login);
    }

    @Nullable
    @Override
    @WebMethod
    public User removeUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        final User currentUser = serviceLocator.getUserService().findById(session.getUserId());
        return serviceLocator.getUserService().removeUserByLogin(currentUser.getLogin(), login);
    }

    @Nullable
    @Override
    @WebMethod
    public List<User> getUserList(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().getList();
    }

    @Override
    @WebMethod
    public void loadUser(
            @WebParam(name = "users", partName = "users") @Nullable List<User> users,
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws Exception {

        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().load(users);
    }

}
